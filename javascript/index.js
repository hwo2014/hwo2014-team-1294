var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log('server host: %s', serverHost);
console.log('server port: %d', serverPort);
console.log('bot name: %s', botName);

// Connect to the server

client = net.connect(serverPort, serverHost, onConnected);

// Send a message to the server

function send(json) {
  client.write(JSON.stringify(json));
  client.write('\n');
};

// When the socket is open, just send a 'join' message

function onConnected() {
  send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
}

// These variables contain information that the pilot know during the race.

var pilotName = null;
var carColor = null;

// Collection of states

var states = {};

states.start = {
  handle: function (message) {
    if (message.msgType === 'join') {
      console.log('pilot has joined the race...');
    }
  },

  next: function (message) {
    return states.join;
  }
};

states.join = {
  handle: function (message) {
    if (message.msgType === 'yourCar') {
      console.log('car information available...');
    }
  },

  next: function (message) {
    return states.yourCar;
  }
};

states.yourCar = {
  handle: function (message) {
    if (message.msgType === 'gameInit') {
      console.log('initializing the game...');
    }
  },

  next: function (message) {
    return states.gameInit;
  }
};

states.gameInit = {
  handle: function (message) {
    if (message.msgType === 'carPositions') {
      console.log('initial positions received...')
    }
  },

  next: function (message) {
    if (message.msgType === 'carPositions') {
      return states.gameStart;
    }
  }
};

states.gameStart = {
  handle: function (message) {
    if (message.msgType === 'gameStart') {
      send({msgType: "throttle", data: 0.5});
    }
  },

  next: function (message) {
    if (message.msgType === 'gameStart') {
      return states.race;
    }
  }
};

// This state controls the racing behavior

states.race = {
  handle: function (message) {
    if (message.msgType === 'carPositions') {
      return send({msgType: "throttle", data: 0.5});
    }

    if (message.msgType === 'gameEnd') {
      return console.log('the game is finished');
    }

    send({msgType: "ping"});
  },

  next: function (message) {
    if (message.msgType === 'gameEnd') {
      return states.gameEnd;
    }
  }
};

states.gameEnd = {
  handle: function (message) {
    if (message.msgType === 'gameStart') {
      send({msgType: "throttle", data: 0.5});
    }

    if (message.msgType === 'tournamentEnd') {
      console.log('the tournament is finished');
    }
  },

  next: function (message) {
    if (message.msgType === 'gameStart') {
      return states.race;
    }
  }
};

// Current state which is in charge

var currentState = states.start;

// Pipe incoming data into the JSON stream

var jsonStream = client.pipe(JSONStream.parse());

// Handle the current message, jump to the next state

jsonStream.on('data', function(data) {
  if (currentState.handle) {
    currentState.handle(data);
  }

  var next = null;

  if (currentState.next) {
    next = currentState.next(data);
  }

  if (next) {
    currentState = next;
  }
});

// The server sent us an error

jsonStream.on('error', function() {
  return console.log("disconnected");
});
